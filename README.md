<h1>ÇAKI</h1>

`caki yedekle` komutu ile depomuzu yedekleyebiliriz. size (yedek.se ya da yedek.tr isimli dosya verecektir.)

`caki kur` komutu ile yedekten mevcud depomuzda bulunmayan paketler kurulur.

`caki tamkur` komutu ile yedekteki tüm paketler kurulur.
