from src import app as App
from src import Main
import sys

import asyncio

if __name__ == '__main__':
	try:
		komut = sys.argv[1:]
		if komut[0] == 'g':
			app = Main.Application()
			app.run(komut[0])
		elif len(komut) > 0:
			App.File(komut[0])
	except:
		print('Bilinmeyen bir hata!\nLütfen Pardus Forum üzerinden @Batuhan_Coskun ile iletişime geçişiniz.')
