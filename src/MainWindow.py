#Bismillahirrahmanirrahim
#Elhamdülillah
import locale
from locale import gettext as _
import os
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import  Gtk
from src.app import File

# Translation Constants:
APPNAME = "caki"
TRANSLATIONS_PATH = "/usr/share/locale"
# SYSTEM_LANGUAGE = os.environ.get("LANG")

# Translation functions:
locale.bindtextdomain(APPNAME, TRANSLATIONS_PATH)
locale.textdomain(APPNAME)
# locale.setlocale(locale.LC_ALL, SYSTEM_LANGUAGE)

class MainWindow(Gtk.Window):
    def __init__(self, application):
        # Gtk Builder
        self.application = application
        self.builder = Gtk.Builder()

        # Translate things on glade:
        self.builder.set_translation_domain(APPNAME)

        self.builder.add_from_file(os.path.dirname(
            os.path.abspath(__file__)) + "/../ui/caki.glade")
        self.builder.connect_signals(self)

        # Add Window
        self.window: Gtk.Window = self.builder.get_object("window")
        self.window.set_position(Gtk.WindowPosition.CENTER)
        self.window.set_application(application)
        self.window.set_default_size(400, 300)
        # self.window.connect('destroy', application.onExit)
        self.window.connect("delete-event", self.on_delete_event)

        self.defineComponents()


        self.window.show_all()





    def defineComponents(self):
        self.btn_about: Gtk.Button = self.builder.get_object("btn_about")
        
        self.bt_add_repo1: Gtk.Button = self.builder.get_object("bt_add_repo1")
        self.bt_add_repo2: Gtk.Button = self.builder.get_object("bt_add_repo2")
        self.bt_add_repo3: Gtk.Button = self.builder.get_object("bt_add_repo3")

        self.bt_delete_repo1: Gtk.Button = self.builder.get_object("bt_delete_repo1")
        self.bt_delete_repo2: Gtk.Button = self.builder.get_object("bt_delete_repo2")
        self.bt_delete_repo3: Gtk.Button = self.builder.get_object("bt_delete_repo3")

        self.bt_edit_repo1: Gtk.Button = self.builder.get_object("bt_edit_repo1")
        self.bt_edit_repo2: Gtk.Button = self.builder.get_object("bt_edit_repo2")
        self.bt_edit_repo3: Gtk.Button = self.builder.get_object("bt_edit_repo3")

        self.bt_apps: Gtk.Button = self.builder.get_object("bt_apps")
        self.bt_backup_pkgs: Gtk.Button = self.builder.get_object("bt_backup_pkgs")
        self.bt_add_app1: Gtk.Button = self.builder.get_object("bt_add_app1")

        self.bt_add_app2: Gtk.Button = self.builder.get_object("bt_add_app2")
        self.bt_install_pkgs: Gtk.Button = self.builder.get_object("bt_install_pkgs")
        self.bt_reinstall_app1: Gtk.Button = self.builder.get_object("bt_reinstall_app1")
        # self.bt_remove_app1: Gtk.Button = self.builder.get_object("bt_remove_app1")
        
        self.bt_remove_app2: Gtk.Button = self.builder.get_object("bt_remove_app2")
        self.bt_full_install_pkgs: Gtk.Button = self.builder.get_object("bt_full_install_pkgs")
        self.bt_reinstall_app2: Gtk.Button = self.builder.get_object("bt_reinstall_app2")
        
        self.bt_reinstall_app3: Gtk.Button = self.builder.get_object("bt_reinstall_app3")
        self.bt_report_app1: Gtk.Button = self.builder.get_object("bt_report_app1")
        self.bt_report_app2: Gtk.Button = self.builder.get_object("bt_report_app2")


    def on_btn_about_clicked(self, b):
        about = Gtk.AboutDialog()
        about.set_program_name("Caki")
        about.set_version("0.1")

    def on_bt_add_repo1_clicked(self, b):
        pass

    def on_bt_add_repo2_clicked(self, b):
        pass

    def on_bt_add_repo3_clicked(self, b):
        pass

    def on_bt_delete_repo1_clicked(self, b):
        pass

    def on_bt_delete_repo2_clicked(self, b):
        pass

    def on_bt_delete_repo3_clicked(self, b):
        pass

    def on_bt_edit_repo1_clicked(self, b):
        pass

    def on_bt_edit_repo2_clicked(self, b):
        pass

    def on_bt_edit_repo3_clicked(self, b):
        pass

    def on_bt_apps_clicked(self, b):
        pass

    def on_bt_backup_pkgs_clicked(self, b):
        File("yedekle")

    def on_bt_add_app1_clicked(self, b):
        pass

    def on_bt_add_app2_clicked(self, b):
        pass

    def on_bt_install_pkgs_clicked(self, b):
        File("kur")

    def on_bt_full_install_pkgs_clicked(self, b):
        File("tamkur")

    def on_bt_remove_app1_clicked(self, b):
        pass

    def on_bt_remove_app2_clicked(self, b):
        pass

    def on_bt_reinstall_app1_clicked(self, b):
        pass

    def on_bt_reinstall_app2_clicked(self, b):
        pass

    def on_bt_reinstall_app3_clicked(self, b):
        pass

    def on_bt_report_app1_clicked(self, b):
        pass

    def on_bt_report_app2_clicked(self, b):
        pass





    def on_delete_event(self, widget, event):
        # it hides the window
        self.window.destroy()
        return True

