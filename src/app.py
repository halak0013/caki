import subprocess
import os
import argparse
import asyncio

link = os.getcwd()+'/'

class File():
	def __init__(self, paket, gui=False):
		self.islem = paket
		if paket == "yedekle":
			self.yedekle(self.__listeAl())
		elif paket == "tamkur":
			yedekDosyasi = input("Yedek dosyasının konumunu belirtiniz: ")
			asyncio.run(self.kur(yedekDosyasi, 0))
		elif paket == "kur":
			yedekDosyasi = input("Yedek dosyasının konumunu belirtiniz: ")
			asyncio.run(self.kur(yedekDosyasi, 1))
		else:
			print("Komut bulunamadı.")
			print("1- tamkur: Tüm yedeklenen depoyu kurar.\n2- kur: eksik paketleri kurar.\n3- yedekle: Depoyu yedekler.")

	def yedekle(self, liste, isim="yedek.se"):
		numara = 0
		f = open(link + isim, "w")

		for paket in liste:
			if numara == 0:
				f.write(f"{paket}")
				numara += 1
			else:
				f.write(f"\n{paket}")
		return "Yedekleme işlemi başarılı."

	async def kur(self, yedekDosyasi, secenek):
		uzantiKontrol = yedekDosyasi.endswith(".tr") or yedekDosyasi.endswith(".se")

		try:
			if not uzantiKontrol:
				return "Bu Çakı'nın yedek dosyası değildir."
			paketler = open(yedekDosyasi, "r").read().split('\n')
			eskiSayi = len(paketler)
		except FileNotFoundError:
			return "Bu isimde herhangi bir dosya bulunamadı."
		
		if len(paketler) == 0:
			return "Dosyanın içeriği boş"
		
		if bool(secenek):
			karsilastir = self.__paketKarsilastir(paketler)
			if len(karsilastir[1]) > 0:
				print("Depoda bulunamayan paket sayısı: ", len(karsilastir[1]))
				if input("Depoda bulunamayan paketleri listelemek ister misiniz(h/E): ").lower() in ["e", "evet", "y", "yes"]:
					self.__listele(karsilastir[1])
			paketler = karsilastir[0]

		if len(paketler) == 0:
			return "Kurulum yapılmaya müsait paket bulunamadı."
		
		await self.__paketleriKur(paketler)
		return True
		
	def __listeAl(self, komut="apt list --installed"):
		cikti = subprocess.getoutput(komut)
		paketler = cikti.split("\n")

		temizPaketler = []

		for paket in paketler:
			eskiPaket = paket
			paket = paket.split('/')[0]
			if (paket != '' and self.islem != "yedekle") or (self.islem == "yedekle" and eskiPaket.endswith(']')):
				temizPaketler.append(paket)

		return temizPaketler

	def __paketSay(self, paketler):
		paketSayisi = len(paketler)
		onay = input(f"{paketSayisi} tane paket kurulacak onaylıyor musunuz(h/E): ").lower()
		
		if onay in ["e", "evet", "y", "yes"] :
			if input("Kurulacak paketleri listelemek ister misiniz(h/E): ").lower() in ["e", "evet", "y", "yes"]:
				self.__listele(paketler)
			return True
		else:
			return False

	def __paketKarsilastir(self, paketler):
		guncelPaketler = self.__listeAl()
		iptalPaketler = []
		sonPaketler = []
		depo = self.__depoPaketleri()

		for paket in paketler:
			if paket in depo:
				if not paket in guncelPaketler:
					sonPaketler.append(paket)
			elif not paket in depo:
				if "" == paket:
					pass
				else:
					iptalPaketler.append(paket)
		return [sonPaketler, iptalPaketler]

	def __depoPaketleri(self):
		return self.__listeAl(komut="apt list --all-versions")

	def __listele(self, paketler):
		sayi = 0
		len(paketler)
		for paket in paketler:
			sayi += 1
			print(sayi, paket)

	async def __paketleriKur(self, paketler):
		sonOnay = self.__paketSay(paketler)
		if sonOnay:
			try:
				komut = "pkexec apt-get -y install"
				for paket in paketler:
					komut += f" {paket}"
				cikti = subprocess.getoutput(komut)
				print(cikti)
				return True
			except KeyboardInterrupt:
				return "Kurulum iptal edildi."
			except KeyboardInterrupt:
				return "Hata, lütfen Pardus Forum'a bildiriniz."
		
		elif not sonOnay:
			return "Kurulum kullanıcı tarafından reddedildi."